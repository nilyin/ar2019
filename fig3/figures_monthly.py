#!/usr/bin/env python3
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import datetime
from dateutil.rrule import rrule, DAILY
import os

oneday    = (datetime.datetime(2016,1,1), datetime.datetime(2016,1,1),  'oneday')
january   = (datetime.datetime(2016,1,1), datetime.datetime(2016,1,31),  '01-Jan')
february  = (datetime.datetime(2016,2,1), datetime.datetime(2016,2,29),  '02-Feb') 
march     = (datetime.datetime(2016,3,1), datetime.datetime(2016,3,31),  '03-Mar')
april     = (datetime.datetime(2016,4,1), datetime.datetime(2016,4,30),  '04-Apr')
may       = (datetime.datetime(2016,5,1), datetime.datetime(2016,5,31),  '05-May')
june      = (datetime.datetime(2016,6,1), datetime.datetime(2016,6,30),  '06-Jun')
july      = (datetime.datetime(2016,7,1), datetime.datetime(2016,7,31),  '07-Jul')
august    = (datetime.datetime(2016,8,1), datetime.datetime(2016,8,31),  '08-Aug')
september = (datetime.datetime(2016,9,1), datetime.datetime(2016,9,30),  '09-Sep')
october   = (datetime.datetime(2016,10,1), datetime.datetime(2016,10,31),'10-Oct')
november  = (datetime.datetime(2016,11,1), datetime.datetime(2016,11,30),'11-Nov')
december  = (datetime.datetime(2016,12,1), datetime.datetime(2016,12,31),'12-Dec')
q1        = (datetime.datetime(2016,1,1), datetime.datetime(2016,3,31),   'Q1')
q2        = (datetime.datetime(2016,4,1), datetime.datetime(2016,6,30),   'Q2')
q3        = (datetime.datetime(2016,7,1), datetime.datetime(2016,9,30),   'Q3')
q4        = (datetime.datetime(2016,10,1), datetime.datetime(2016,12,31), 'Q4')
year      = (datetime.datetime(2016,1,1), datetime.datetime(2016,12,31), 'Year')


periods = [january, february, march, april, may, june, july, august, september, october, november, december]
#periods = [q1, q2 , q3, q4]
#periods = [year]
#periods = [oneday]


month_index = 0

sp_0 = np.zeros(len(periods))
sp_0_l = np.zeros(len(periods))
sp_0_o = np.zeros(len(periods))

sp_17 = np.zeros(len(periods))
sp_17_l = np.zeros(len(periods))
sp_17_o = np.zeros(len(periods))



for period in periods:
    name = period[2]
    print (name)


    sp_0[month_index]  = np.sum(np.load('Histo-Sprites_0-' + name + '.npy'))/1000
    sp_17[month_index] = np.sum(np.load('Histo-Sprites_17-' + name + '.npy'))/1000

    sp_0_l[month_index]  = np.sum(np.load('Histo-Sprites_0-l-' + name + '.npy'))/1000
    sp_17_l[month_index] = np.sum(np.load('Histo-Sprites_17-l-' + name + '.npy'))/1000

    sp_0_o[month_index]  = np.sum(np.load('Histo-Sprites_0-o-' + name + '.npy'))/1000
    sp_17_o[month_index] = np.sum(np.load('Histo-Sprites_17-o-' + name + '.npy'))/1000


    month_index += 1

    

plt.rcParams.update({'font.size': 11})
fig, ax = plt.subplots()

p1 = plt.bar(np.arange(0,24,2), sp_17_l, color = 'k')
p2 = plt.bar(np.arange(1,24,2), sp_17_o, color = 'b')


plt.title('Monthly distribution of sprites')
plt.ylabel('Number of sprites, $\\times 10^3$')

#plt.yticks(np.arange(0,2000,250)) # for zero iso
plt.yticks(np.arange(0,3000,250))
plt.xticks(np.arange(0.5,24,2), ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'))
#plt.grid(True)
plt.legend((p1[0], p2[0]), ('land', 'ocean'))
plt.savefig('fig3.png')
plt.savefig('fig3.eps')
#plt.show()


