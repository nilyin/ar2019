#!/usr/bin/env python3
import pandas as pd
import numpy as np
import datetime
from dateutil.rrule import rrule, DAILY
import os

oneday    = (datetime.datetime(2016,1,1), datetime.datetime(2016,1,25),  'oneday')
january   = (datetime.datetime(2016,1,1), datetime.datetime(2016,1,31),  '01-Jan')
february  = (datetime.datetime(2016,2,1), datetime.datetime(2016,2,29),  '02-Feb') 
march     = (datetime.datetime(2016,3,1), datetime.datetime(2016,3,31),  '03-Mar')
april     = (datetime.datetime(2016,4,1), datetime.datetime(2016,4,30),  '04-Apr')
may       = (datetime.datetime(2016,5,1), datetime.datetime(2016,5,31),  '05-May')
june      = (datetime.datetime(2016,6,1), datetime.datetime(2016,6,30),  '06-Jun')
july      = (datetime.datetime(2016,7,1), datetime.datetime(2016,7,31),  '07-Jul')
august    = (datetime.datetime(2016,8,1), datetime.datetime(2016,8,31),  '08-Aug')
september = (datetime.datetime(2016,9,1), datetime.datetime(2016,9,30),  '09-Sep')
october   = (datetime.datetime(2016,10,1), datetime.datetime(2016,10,31),'10-Oct')
november  = (datetime.datetime(2016,11,1), datetime.datetime(2016,11,30),'11-Nov')
december  = (datetime.datetime(2016,12,1), datetime.datetime(2016,12,31),'12-Dec')
q1        = (datetime.datetime(2016,1,1), datetime.datetime(2016,3,31),   'Q1')
q2        = (datetime.datetime(2016,4,1), datetime.datetime(2016,6,30),   'Q2')
q3        = (datetime.datetime(2016,7,1), datetime.datetime(2016,9,30),   'Q3')
q4        = (datetime.datetime(2016,10,1), datetime.datetime(2016,12,31), 'Q4')
year      = (datetime.datetime(2016,1,1), datetime.datetime(2016,12,31), 'Year')


#periods = [january, february, march, april, may, june, july, august, september, october, november, december]
periods = [september, october, november, december]
#periods = [q1, q2 , q3, q4]
#periods = [january, february, march]


def sprite_probability(moment):
    if moment <= 100:
        result = 0
    elif 100 < moment <= 500:
        result = 0.0025 * moment - 0.25
    elif moment > 500:
        result = 1

#    if moment <= 100:
#        result = 0.00
#    elif 100 < moment <= 150:
#        result = 0.05
#    elif 150 < moment <= 200:
#        result = 0.20
#    elif 200 < moment <= 250:
#        result = 0.40
#    elif 250 < moment <= 300:
#        result = 0.65
#    elif 300 < moment <= 400:
#        result = 0.80
#    elif 400 < moment <= 500:
#        result = 0.85
#    else:
#        result = 1.00

    return result


for period in periods:
    name = period[2]
    print (name)

    start_day, end_day = period[0], period[1] 
    result_0 = np.zeros([360,720])
    result_17 = np.zeros([360,720])

    for eventday in rrule(DAILY, dtstart=start_day, until=end_day):
    
        filename = os.path.join(os.getenv('PWD'),'data',eventday.strftime('DATA-%Y%m%d.loc'))
        print(filename)
        data = pd.read_csv(filename)
        data = data.loc[data['night'] == 1]

        for index, row in data.iterrows():

            if row.lon >= 0:
                lat,lon = int(2*(row.lat + 90)), int(2*(row.lon))
            else:
                lat,lon = int(2*(row.lat + 90)), int(2*(row.lon + 360))

            result_0[lat,lon] += sprite_probability(row.Moment_0)
            result_17[lat,lon] += sprite_probability(row.Moment_17)

    print('00-',np.sum(result_0))
    print('17-',np.sum(result_17))
    
    result_0[::-1,:].tofile(name + '_0')
    result_17[::-1,:].tofile(name + '_17')
#    os.environ["SPRITE"] = name
#    os.system('ncl -Q figgen.ncl')
#    os.system('convert -trim +repage ' + name +'.png ' + name + '.png') 
 

