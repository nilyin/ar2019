#!/usr/bin/env python3
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import datetime
from dateutil.rrule import rrule, DAILY
import os

oneday    = (datetime.datetime(2016,1,1), datetime.datetime(2016,1,2),  'oneday')
january   = (datetime.datetime(2016,1,1), datetime.datetime(2016,1,31),  '01-Jan')
february  = (datetime.datetime(2016,2,1), datetime.datetime(2016,2,29),  '02-Feb') 
march     = (datetime.datetime(2016,3,1), datetime.datetime(2016,3,31),  '03-Mar')
april     = (datetime.datetime(2016,4,1), datetime.datetime(2016,4,30),  '04-Apr')
may       = (datetime.datetime(2016,5,1), datetime.datetime(2016,5,31),  '05-May')
june      = (datetime.datetime(2016,6,1), datetime.datetime(2016,6,30),  '06-Jun')
july      = (datetime.datetime(2016,7,1), datetime.datetime(2016,7,31),  '07-Jul')
august    = (datetime.datetime(2016,8,1), datetime.datetime(2016,8,31),  '08-Aug')
september = (datetime.datetime(2016,9,1), datetime.datetime(2016,9,30),  '09-Sep')
october   = (datetime.datetime(2016,10,1), datetime.datetime(2016,10,31),'10-Oct')
november  = (datetime.datetime(2016,11,1), datetime.datetime(2016,11,30),'11-Nov')
december  = (datetime.datetime(2016,12,1), datetime.datetime(2016,12,31),'12-Dec')
q1        = (datetime.datetime(2016,1,1), datetime.datetime(2016,3,31),   'Q1')
q2        = (datetime.datetime(2016,4,1), datetime.datetime(2016,6,30),   'Q2')
q3        = (datetime.datetime(2016,7,1), datetime.datetime(2016,9,30),   'Q3')
q4        = (datetime.datetime(2016,10,1), datetime.datetime(2016,12,31), 'Q4')
year      = (datetime.datetime(2016,1,1), datetime.datetime(2016,12,31), 'Year')


periods = [january, february, march, april, may, june, july, august, september, october, november, december]
#periods = [q1, q2 , q3, q4]
#periods = [year]
#periods = [oneday]

path = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'data'))

land = np.load(os.path.join(path,'land.npy'))
ocean = np.load(os.path.join(path,'ocean.npy'))

for period in periods:
    name = period[2]
    print (name)

    start_day, end_day = period[0], period[1] 

    number =  np.zeros((1), dtype = int)

    for eventday in rrule(DAILY, dtstart=start_day, until=end_day):
    
        filename = os.path.join(path, eventday.strftime('DATA-%Y%m%d.loc'))
        print(filename)
        data = pd.read_csv(filename)
        data = data.loc[data['night'] == 1]

        number += data.shape[0]


    np.savetxt('Total-Flash-Number-' + name, number)

