#!/usr/bin/env python3
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import datetime
from dateutil.rrule import rrule, DAILY
import os

oneday    = (datetime.datetime(2016,1,1), datetime.datetime(2016,1,1),  'oneday')
january   = (datetime.datetime(2016,1,1), datetime.datetime(2016,1,31),  '01-Jan')
february  = (datetime.datetime(2016,2,1), datetime.datetime(2016,2,29),  '02-Feb') 
march     = (datetime.datetime(2016,3,1), datetime.datetime(2016,3,31),  '03-Mar')
april     = (datetime.datetime(2016,4,1), datetime.datetime(2016,4,30),  '04-Apr')
may       = (datetime.datetime(2016,5,1), datetime.datetime(2016,5,31),  '05-May')
june      = (datetime.datetime(2016,6,1), datetime.datetime(2016,6,30),  '06-Jun')
july      = (datetime.datetime(2016,7,1), datetime.datetime(2016,7,31),  '07-Jul')
august    = (datetime.datetime(2016,8,1), datetime.datetime(2016,8,31),  '08-Aug')
september = (datetime.datetime(2016,9,1), datetime.datetime(2016,9,30),  '09-Sep')
october   = (datetime.datetime(2016,10,1), datetime.datetime(2016,10,31),'10-Oct')
november  = (datetime.datetime(2016,11,1), datetime.datetime(2016,11,30),'11-Nov')
december  = (datetime.datetime(2016,12,1), datetime.datetime(2016,12,31),'12-Dec')
q1        = (datetime.datetime(2016,1,1), datetime.datetime(2016,3,31),   'Q1')
q2        = (datetime.datetime(2016,4,1), datetime.datetime(2016,6,30),   'Q2')
q3        = (datetime.datetime(2016,7,1), datetime.datetime(2016,9,30),   'Q3')
q4        = (datetime.datetime(2016,10,1), datetime.datetime(2016,12,31), 'Q4')
year      = (datetime.datetime(2016,1,1), datetime.datetime(2016,12,31), 'Year')


periods = [january, february, march, april, may, june, july, august, september, october, november, december]
#periods = [q1, q2 , q3, q4]
#periods = [year]
#periods = [oneday]



for period in periods:
    name = period[2]
    print (name)

    x = np.arange(1000)
    R_current =  np.load('Histo-Current-' + name +'.npy')/1000
    R_current_l =  np.load('Histo-Current-l-' + name +'.npy')/1000
    R_current_o =  np.load('Histo-Current-o-' + name +'.npy')/1000

    plt.rcParams.update({'font.size': 14})
    plt.plot(x[15:210], R_current_l[15:210], '-k', label='Land', linewidth=3)
    plt.plot(x[15:210], R_current_o[15:210], '-b', label='Ocean', linewidth=3)
#    plt.yscale('symlog')
    plt.title('WWLLN peak current distribution')
    plt.ylabel('Number of flashes, $\\times 10^3$')
    plt.xlabel('Peak current, kA')
    plt.grid(True)
    plt.xticks(np.arange(15,210,30))
    plt.legend()
#    plt.savefig('fig1.eps')
    plt.savefig('fig-' + name + '.png')
#    plt.show()
    plt.close()
