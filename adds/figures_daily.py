#!/usr/bin/env python3
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import datetime
from dateutil.rrule import rrule, DAILY
import os

oneday    = (datetime.datetime(2016,1,1), datetime.datetime(2016,1,1),  'oneday')
january   = (datetime.datetime(2016,1,1), datetime.datetime(2016,1,31),  '01-Jan')
february  = (datetime.datetime(2016,2,1), datetime.datetime(2016,2,29),  '02-Feb') 
march     = (datetime.datetime(2016,3,1), datetime.datetime(2016,3,31),  '03-Mar')
april     = (datetime.datetime(2016,4,1), datetime.datetime(2016,4,30),  '04-Apr')
may       = (datetime.datetime(2016,5,1), datetime.datetime(2016,5,31),  '05-May')
june      = (datetime.datetime(2016,6,1), datetime.datetime(2016,6,30),  '06-Jun')
july      = (datetime.datetime(2016,7,1), datetime.datetime(2016,7,31),  '07-Jul')
august    = (datetime.datetime(2016,8,1), datetime.datetime(2016,8,31),  '08-Aug')
september = (datetime.datetime(2016,9,1), datetime.datetime(2016,9,30),  '09-Sep')
october   = (datetime.datetime(2016,10,1), datetime.datetime(2016,10,31),'10-Oct')
november  = (datetime.datetime(2016,11,1), datetime.datetime(2016,11,30),'11-Nov')
december  = (datetime.datetime(2016,12,1), datetime.datetime(2016,12,31),'12-Dec')
q1        = (datetime.datetime(2016,1,1), datetime.datetime(2016,3,31),   'Q1')
q2        = (datetime.datetime(2016,4,1), datetime.datetime(2016,6,30),   'Q2')
q3        = (datetime.datetime(2016,7,1), datetime.datetime(2016,9,30),   'Q3')
q4        = (datetime.datetime(2016,10,1), datetime.datetime(2016,12,31), 'Q4')
year      = (datetime.datetime(2016,1,1), datetime.datetime(2016,12,31), 'Year')


periods = [year, january, february, march, april, may, june, july, august, september, october, november, december]
#periods = [q1, q2 , q3, q4]
#periods = [year]
#periods = [oneday]

def adder(array):
    return np.append(array, array[:5])

def reducer(array):
    return np.add.reduceat(array, np.arange(0, len(array), 7))


for period in periods:
    name = period[2]
    print (name)


    SP_number_0  = np.load('Daily-Sprites_0-' + name + '.npy')
    SP_number_17 = np.load('Daily-Sprites_17-' + name + '.npy')

    SP_number_0_l  = np.load('Daily-Sprites_0-l-' + name + '.npy')
    SP_number_17_l = np.load('Daily-Sprites_17-l-' + name + '.npy')

    SP_number_0_o  = np.load('Daily-Sprites_0-o-' + name + '.npy')
    SP_number_17_o = np.load('Daily-Sprites_17-o-' + name + '.npy')


    
    SP_number_17_l = reducer(adder(SP_number_17_l))
    SP_number_17_o = reducer(adder(SP_number_17_o))

    SP_number_0_l = reducer(adder(SP_number_0_l))
    SP_number_0_o = reducer(adder(SP_number_0_o))


    fig, ax = plt.subplots()

#    plt.bar(np.arange(0,106,2), SP_number_0_l)
#    plt.bar(np.arange(1,106,2), SP_number_0_o)

#    plt.plot(SP_number_0, label='Sprites 0-iso')
#    plt.plot(SP_number_17, label='Sprites 17-iso')

    plt.plot(SP_number_0_l, '-k' , label='Sprites 0-iso LAND')
#    plt.plot(SP_number_17_l, '-k', label='Sprites 17-iso LAND')


    plt.plot(SP_number_0_o, '--b',  label='Sprites 0-iso OCEAN')
 #   plt.plot(SP_number_17_o, '-b', label='Sprites 17-iso OCEAN')



    plt.grid(True)
    plt.legend()
    plt.show()
#    plt.savefig('histo.png')


