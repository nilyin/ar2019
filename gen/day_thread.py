#!/usr/bin/env python3
import sys, os
from netCDF4 import Dataset 
from wrf import getvar, vinterp
import pandas as pd
import numpy as np
from datetime import date, timedelta
import ephem
import math


eventday = date(int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]))
print(eventday)
if date(2016,1,1) <= eventday <= date(2016,12,31):
    nz,nlat,nlon = 45,360,720
    sun_height   = 70000
    sun          = ephem.Sun()
    observer     = ephem.Observer()
    mainpath     = os.path.join(os.getenv('HOME'),'wrfmain')
    wwllnfile    = os.path.join(mainpath, 'data', 'wwlln', eventday.strftime('%Y'), eventday.strftime('%m'), eventday.strftime('AE%Y%m%d.loc'))
    resultfile   = os.path.join(os.getenv('PWD'),'data', eventday.strftime('DATA-%Y%m%d.loc'))
    eventday    += timedelta(days=-1)
    wrffile      = os.path.join(mainpath, 'global', 'RDA_DS084.1', eventday.strftime('%Y%m%d18'), eventday.strftime('wrfout_d01_%Y-%m-%d_18:00:00'))
    ncfile       = Dataset(wrffile)
    timeindex    = range(18,90)
    tk           = getvar(ncfile, "tk", timeidx=timeindex, meta=False)
    tk           = np.mean(tk, axis=0)
    levels       = np.arange(0,10,0.05)
    newtk        = vinterp(ncfile, tk, 'ght_agl', extrapolate=True, field_type='tk',interp_levels=levels)
    dipol_0      = 50 * np.argmax(np.sign(newtk-273.15)<0, axis=0)
    dipol_17     = 50 * np.argmax(np.sign(newtk-273.15+17)<0, axis=0)

    data = pd.read_csv(wwllnfile, names=['date', 'time', 'lat', 'lon', 'err','power', 'energy', 'deviation', 'n'])
    data['energy'] = data['energy'] -  data['deviation']
    data = data.loc[ data['energy'] > 100.0]
    data['datetime'] = np.array(data.date.astype(str))+"_"+np.array(data.time.astype(str))
    data['fulltime'] = pd.to_datetime(data.datetime, format='%Y/%m/%d_%H:%M:%S.%f') 
    data['night'] = 0    
    data['Mcurrent'] = np.power(np.array(data['energy'] / 2.22908), (1./1.62)) 

    for index, row in data.iterrows():
        if row.lon >= 0:
            lat,lon = int(2*(row.lat + 90)), int(2*(row.lon))
        else:
            lat,lon = int(2*(row.lat + 90)), int(2*(row.lon + 360))
            
        data.at[index, 'dipol_17']  = dipol_17[lat,lon]
        data.at[index, 'dipol_0']  = dipol_0[lat,lon]
        data.at[index, 'Charge'] = 0.68 * row.Mcurrent 

        observer.lat, observer.lon, observer.elevation = str(row.lat), str(row.lon), sun_height
        observer.date = row.fulltime
        sun.compute(observer)
        current_sun_alt = sun.alt
        if current_sun_alt*180/math.pi <= 0:
            data.at[index,'night'] = 1 

    data['Moment_0'] = data['Charge'] * data['dipol_0'] * 1e-3
    data['Moment_17'] = data['Charge'] * data['dipol_17'] * 1e-3

    data = data.loc[data['night'] == 1]
    data = data.loc[data['dipol_0'] > 0]
    data = data.loc[data['dipol_17'] > 0]

    del data['err']
    del data['power']
    del data['deviation']
    del data['n']
    del data['datetime'] 
    del data['fulltime']

    data.to_csv(resultfile, index=False, date_format = '%Y/%m/%d_%H:%M')

