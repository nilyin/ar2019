#!/usr/bin/env python3
from subprocess import Popen   
from datetime import date, timedelta

startday = date(2016,1,1)
endday = date(2016,12,31)
day = startday
day_number = (endday - startday).days    

print(day_number)
thread_number = 16  
for i_block in range(day_number // thread_number +1):
    threads = []
    for i in range(thread_number):
        threads.append(Popen(['./day_thread.py', day.strftime('%Y'), day.strftime('%m'), day.strftime('%d')]))    
        day += timedelta(hours=24)
    for i in range(thread_number):
        threads[i].wait()
