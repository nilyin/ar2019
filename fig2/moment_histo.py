#!/usr/bin/env python3
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import datetime
from dateutil.rrule import rrule, DAILY
import os

oneday    = (datetime.datetime(2016,1,1), datetime.datetime(2016,1,1),  'oneday')
january   = (datetime.datetime(2016,1,1), datetime.datetime(2016,1,31),  '01-Jan')
february  = (datetime.datetime(2016,2,1), datetime.datetime(2016,2,29),  '02-Feb') 
march     = (datetime.datetime(2016,3,1), datetime.datetime(2016,3,31),  '03-Mar')
april     = (datetime.datetime(2016,4,1), datetime.datetime(2016,4,30),  '04-Apr')
may       = (datetime.datetime(2016,5,1), datetime.datetime(2016,5,31),  '05-May')
june      = (datetime.datetime(2016,6,1), datetime.datetime(2016,6,30),  '06-Jun')
july      = (datetime.datetime(2016,7,1), datetime.datetime(2016,7,31),  '07-Jul')
august    = (datetime.datetime(2016,8,1), datetime.datetime(2016,8,31),  '08-Aug')
september = (datetime.datetime(2016,9,1), datetime.datetime(2016,9,30),  '09-Sep')
october   = (datetime.datetime(2016,10,1), datetime.datetime(2016,10,31),'10-Oct')
november  = (datetime.datetime(2016,11,1), datetime.datetime(2016,11,30),'11-Nov')
december  = (datetime.datetime(2016,12,1), datetime.datetime(2016,12,31),'12-Dec')
q1        = (datetime.datetime(2016,1,1), datetime.datetime(2016,3,31),   'Q1')
q2        = (datetime.datetime(2016,4,1), datetime.datetime(2016,6,30),   'Q2')
q3        = (datetime.datetime(2016,7,1), datetime.datetime(2016,9,30),   'Q3')
q4        = (datetime.datetime(2016,10,1), datetime.datetime(2016,12,31), 'Q4')
year      = (datetime.datetime(2016,1,1), datetime.datetime(2016,12,31), 'Year')


periods = [january, february, march, april, may, june, july, august, september, october, november, december]
#periods = [q1, q2 , q3, q4]
#periods = [year]
#periods = [oneday]

path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'data'))

land = np.load(os.path.join(path, 'land.npy'))
ocean = np.load(os.path.join(path, 'ocean.npy'))


def sprite_probability(moment):
    if moment <= 100:
        result = 0
    elif 100 < moment <= 500:
        result = 0.0025 * moment - 0.25
    elif moment > 500:
        result = 1
    return result


for period in periods:
    name = period[2]
    print (name)

    start_day, end_day = period[0], period[1] 

    R_moment_0 =  np.zeros(3000)
    R_moment_17 =  np.zeros(3000)
    SP_number_0 =  np.zeros(3000)
    SP_number_17 =  np.zeros(3000)

    R_moment_0_l = np.zeros(3000)
    R_moment_17_l = np.zeros(3000)
    SP_number_0_l =  np.zeros(3000)
    SP_number_17_l =  np.zeros(3000)

    R_moment_0_o = np.zeros(3000)
    R_moment_17_o = np.zeros(3000)
    SP_number_0_o =  np.zeros(3000)
    SP_number_17_o =  np.zeros(3000)

    x_moment = np.arange(0, 3000, 1)

    for eventday in rrule(DAILY, dtstart=start_day, until=end_day):
    
        filename = os.path.join(path, eventday.strftime('DATA-%Y%m%d.loc'))
        print(filename)
        data = pd.read_csv(filename)
        data = data.loc[data['night'] == 1]

        for i, row in data.iterrows():

            if row.lon >= 0:
                lat,lon = int(2*(row.lat +90)), int (2*(row.lon))
            else:
                lat,lon = int(2*(row.lat +90)), int (2*(row.lon + 360))

            c_index = int(row.Moment_0 // 1)
            d_index = int(row.Moment_17 // 1)

            if c_index < 3000:
                R_moment_0[c_index] +=1
                SP_number_0[c_index] += sprite_probability(row.Moment_0)

                R_moment_0_l[c_index] += land[lat,lon]       
                SP_number_0_l[c_index] += sprite_probability(row.Moment_0) * land[lat,lon]

                R_moment_0_o[c_index] += ocean[lat,lon]       
                SP_number_0_o[c_index] += sprite_probability(row.Moment_0) * ocean[lat,lon]       


            if d_index < 3000:
                R_moment_17[d_index] +=1
                SP_number_17[d_index] += sprite_probability(row.Moment_17)

                R_moment_17_l[d_index] += land[lat,lon]
                SP_number_17_l[d_index] += sprite_probability(row.Moment_17) * land[lat,lon]

                R_moment_17_o[d_index] += ocean[lat,lon]
                SP_number_17_o[d_index] += sprite_probability(row.Moment_17) * ocean[lat,lon]


    np.save('Histo-Moment_0-' + name, R_moment_0)
    np.save('Histo-Sprites_0-' + name, SP_number_0)
    np.save('Histo-Moment_17-' + name, R_moment_17)
    np.save('Histo-Sprites_17-' + name, SP_number_17)

    np.save('Histo-Moment_0-l-' + name, R_moment_0_l)
    np.save('Histo-Sprites_0-l-' + name, SP_number_0_l)
    np.save('Histo-Moment_17-l-' + name, R_moment_17_l)
    np.save('Histo-Sprites_17-l-' + name, SP_number_17_l)

    np.save('Histo-Moment_0-o-' + name, R_moment_0_o)
    np.save('Histo-Sprites_0-o-' + name, SP_number_0_o)
    np.save('Histo-Moment_17-o-' + name, R_moment_17_o)
    np.save('Histo-Sprites_17-o-' + name, SP_number_17_o)



