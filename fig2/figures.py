#!/usr/bin/env python3
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import datetime
from dateutil.rrule import rrule, DAILY
import os

oneday    = (datetime.datetime(2016,1,1), datetime.datetime(2016,1,1),  'oneday')
january   = (datetime.datetime(2016,1,1), datetime.datetime(2016,1,31),  '01-Jan')
february  = (datetime.datetime(2016,2,1), datetime.datetime(2016,2,29),  '02-Feb') 
march     = (datetime.datetime(2016,3,1), datetime.datetime(2016,3,31),  '03-Mar')
april     = (datetime.datetime(2016,4,1), datetime.datetime(2016,4,30),  '04-Apr')
may       = (datetime.datetime(2016,5,1), datetime.datetime(2016,5,31),  '05-May')
june      = (datetime.datetime(2016,6,1), datetime.datetime(2016,6,30),  '06-Jun')
july      = (datetime.datetime(2016,7,1), datetime.datetime(2016,7,31),  '07-Jul')
august    = (datetime.datetime(2016,8,1), datetime.datetime(2016,8,31),  '08-Aug')
september = (datetime.datetime(2016,9,1), datetime.datetime(2016,9,30),  '09-Sep')
october   = (datetime.datetime(2016,10,1), datetime.datetime(2016,10,31),'10-Oct')
november  = (datetime.datetime(2016,11,1), datetime.datetime(2016,11,30),'11-Nov')
december  = (datetime.datetime(2016,12,1), datetime.datetime(2016,12,31),'12-Dec')
q1        = (datetime.datetime(2016,1,1), datetime.datetime(2016,3,31),   'Q1')
q2        = (datetime.datetime(2016,4,1), datetime.datetime(2016,6,30),   'Q2')
q3        = (datetime.datetime(2016,7,1), datetime.datetime(2016,9,30),   'Q3')
q4        = (datetime.datetime(2016,10,1), datetime.datetime(2016,12,31), 'Q4')
year      = (datetime.datetime(2016,1,1), datetime.datetime(2016,12,31), 'Year')


periods = [january, february, march, april, may, june, july, august, september, october, november, december]
#periods = [q1, q2 , q3, q4]
#periods = [year]
#periods = [oneday]


for period in periods:
    name = period[2]
    print (name)


    R_moment_0   = np.load('Histo-Moment_0-' + name + '.npy')/1000
    SP_number_0  = np.load('Histo-Sprites_0-' + name + '.npy')/1000
    R_moment_17  = np.load('Histo-Moment_17-' + name + '.npy')/1000
    SP_number_17 = np.load('Histo-Sprites_17-' + name + '.npy')/1000

    R_moment_0_l   = np.load('Histo-Moment_0-l-' + name + '.npy')/1000
    SP_number_0_l  = np.load('Histo-Sprites_0-l-' + name + '.npy')/1000
    R_moment_17_l  = np.load('Histo-Moment_17-l-' + name + '.npy')/1000
    SP_number_17_l = np.load('Histo-Sprites_17-l-' + name + '.npy')/1000

    R_moment_0_o   = np.load('Histo-Moment_0-o-' + name + '.npy')/1000
    SP_number_0_o  = np.load('Histo-Sprites_0-o-' + name + '.npy')/1000
    R_moment_17_o  = np.load('Histo-Moment_17-o-' + name + '.npy')/1000
    SP_number_17_o = np.load('Histo-Sprites_17-o-' + name + '.npy')/1000


    cutoff = 1000


    plt.rcParams.update({'font.size' : 14})
    plt.figure()
#    plt.plot(R_moment_0[:cutoff], label='ICM 0-iso')
#    plt.plot(SP_number_0[:cutoff], label='Sprites 0-iso')
#    plt.plot(R_moment_17[:cutoff], label='ICM 17-iso')
#    plt.plot(SP_number_17[:cutoff], label='Sprites 17-iso')

#    plt.plot(R_moment_0_l[:cutoff], '--r' , label='ICM 0-iso LAND')
#    plt.plot(SP_number_0_l[:cutoff], '-r' , label='Sprites 0-iso LAND')
    plt.plot(R_moment_17_l[:cutoff], '--k', label='flashes (land)', linewidth=3)
    plt.plot(SP_number_17_l[:cutoff], '-k', label='sprites (land)', linewidth=3)

#    plt.plot(R_moment_0_o[:cutoff], '--g',  label='ICM 0-iso OCEAND')
#    plt.plot(SP_number_0_o[:cutoff], '-g',  label='Sprites 0-iso OCEAN')
    plt.plot(R_moment_17_o[:cutoff], '--b', label='flashes (ocean)', linewidth=3)
    plt.plot(SP_number_17_o[:cutoff], '-b', label='sprites (ocean)', linewidth=3)


    plt.ylabel('Number, $\\times 10^3$')
    plt.xlabel('Impulce current moment (ICM), C $\\times$ km')
    plt.grid(True)
    plt.legend()
#    plt.show()
    plt.savefig(name + '-ICM.png')
#    plt.savefig('fig2.eps')
    plt.close()

